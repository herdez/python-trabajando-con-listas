## Trabajando con listas

Definir las funciones necesarias para que las comparaciones en el `driver code`
sean `True`.

```python
"""function to return the first n elements of the list"""



"""function to drop the first n elements of the list and return the rest"""



"""function to add 'element' to the end of the list"""



"""function to add 'element' to the beginning of the list"""



"""function to add 'element' at position 'index' to the list"""



"""function to add any two elements to the list at the index"""



"""function to delete the element from the end of the list and return the deleted element"""



"""function to delete the element at the beginning of the list"""



"""function to delete the element at the position 'index'"""



"""function to delete the element of the list where element = val"""





#driver code

print(first_n([1, 4, 3, 6, 5], 3) == [1, 4, 3])

print(drop_n([1, 4, 3, 6, 5], 3) == [6, 5])

print(end_lis_add([1, 4, 3, 6, 5], 20) == [1, 4, 3, 6, 5, 20])

print(begin_lis_add([1, 4, 3, 6, 5], 34) == [34, 1, 4, 3, 6, 5])

print(index_lis_add([1, 4, 3, 6, 5], 2, 100) == [1, 4, 100, 3, 6, 5])

print(index_lis_multiple_add([1, 4, 3, 6, 5], 3, 34, 23) == [1, 4, 3, 34, 23, 6, 5])

print(end_list_delete([1, 4, 3, 6, 5]) == 5)

print(start_lis_delete([1, 4, 3, 6, 5]) == [4, 3, 6, 5])

print(delete_at_lis([1, 4, 3, 6, 5], 2) == [1, 4, 6, 5])

print(remove_val_lis([1, 4, 3, 6, 5], 6) == [1, 4, 3, 5])


```
